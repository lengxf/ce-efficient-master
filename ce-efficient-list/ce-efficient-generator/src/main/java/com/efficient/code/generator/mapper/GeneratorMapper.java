package com.efficient.code.generator.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Lgq.
 * @date 2018/12/26 9:52
 */
public interface GeneratorMapper {

    List<Map<String, Object>> queryList(Map<String, Object> map);

    int queryTotal(Map<String, Object> map);

    Map<String, String> queryTable(String tableName);

    List<Map<String, String>> queryColumns(String tableName);

}
